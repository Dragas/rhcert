#!/bin/bash -xe                                                                 
                                                                                
mkdir archive backup                                                            
cd /var/log                                                                     
tar --create --verbose --file ~/archive/log.tar *.log                           
cd -                                                                            
cd archive                                                                      
tar --file log.tar --list                                                       
tar --extract --verbose --file log.tar --directory ~/backup                     
cd ../backup                                                                    
ls
