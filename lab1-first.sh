#!/bin/bash                                                                     
set -ex # lab 2 requires to fail on existing group                                                                         
                                                                                
#provide departaments as args                                                   
for it in $@                                                                    
do                                                                              
    mkdir "/${it}"                                                              
    groupadd "${it}"                                                            
    useradd -s "/bin/bash" -g "${it}" "${it}-admin"                             
    for usr in "first" "second"                                                 
    do                                                                          
        useradd -s "/bin/bash" -g "${it}" "${it}-${usr}"                        
    done                                                                        
    chmod -R 1770 "/${it}"                                                      
    echo "This file contains confidential information for the department" > "/${it}/document"
    chmod 640 "/${it}/document"                                                 
    chown -R "${it}-admin":"${it}" "/${it}"                                     
done 
